# Souls Store

## Context
This project was created as a Jam participation in Ludum Dare 44 by the group of [Scream School](https://screamschool.ru) Unity Pro spring 2019 course students.

## Play
- Download build from [itch.io]()
- Leave a comment on our [ludum dare page]()

## Team
| Name | Role | BitBucket profile | Ludum Dare profile |
|:---:|:---:|:---:|:---:|
| Ярослав Кравцов | Team supervisor | [YaroslavKravtsov](https://bitbucket.org/YaroslavKravtsov) | []() |
| Антон Новиков |  | [novikovantonbox](https://bitbucket.org/novikovantonbox) | [anton-novikov](https://ldjam.com/users/anton-novikov) |
| Артём Иродов |  | [irodovartem](https://bitbucket.org/irodovartem) | [irodovartem](https://ldjam.com/users/irodovartem) |
| Евгений Байдалинов |  | [rollnoir](https://bitbucket.org/rollnoir) | []() |
| Дмитрий Вдовин |  | [vdoff](https://bitbucket.org/vdoff) | []() |
| Андрей Гапонов |  | [Nekronchik](https://bitbucket.org/Nekronchik) | [andrey-gaponov](https://ldjam.com/users/andrey-gaponov) |
| Алексей Лебедев |  | [Mefisto999](https://bitbucket.org/Mefisto999) | []() |
| Матвей Полётин |  | [matthewpoletin](http://bitbucket.org/matthewpoletin) | [matthewpoletin](https://ldjam.com/users/matthewpoletin) |
| Павел Руднев |  | [strippett](https://bitbucket.org/strippett) | []() |
| Александр Чучалов |  | []() | []() |

## Setup
- Download and install [Unity 2018.3.4f1](https://unity3d.com/ru/get-unity/download/archive)
- Clone repository
```bash
git clone git@bitbucket.org:ssupld44/ld44.git
cd ld44
```
- Open project folder in Unity editor

## Controls
- A, D - Move camera
- Space - Pause / Unpause
- Esc - Pause menu
