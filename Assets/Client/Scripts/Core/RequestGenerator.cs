using System.Collections;
using System.Linq;
using Client.Scripts.MovementAI;
using UnityEngine;

namespace Client.Scripts.Core
{
    public class RequestGenerator : MonoBehaviour
    {
        #region Singleton

        public static RequestGenerator Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public float timeToGenerateSeconds = 5.0f;

        private void Start()
        {
            StartCoroutine(GenerateRequests());
        }

        private IEnumerator GenerateRequests()
        {
            while (true)
            {
                // Ожидание таймаута
                yield return new WaitForSeconds(timeToGenerateSeconds);

                // Выбор персонажа
                var humans = GameObject.FindGameObjectsWithTag("Humans");
                if (humans.Length <= 0)
                    continue;
                var availableHumans = humans.ToList().Where((human, index) =>
                    human.GetComponent<MovementSystem>().State == HumanState.GoToPoi ||
					human.GetComponent<MovementSystem>().State == HumanState.Idle
				).ToArray();
                if (availableHumans.Length <= 0)
                    continue;

                var selectedHuman = availableHumans[Random.Range(0, availableHumans.Length)];

                // Отправка персонажа к статуи
                selectedHuman.GetComponent<MovementSystem>().State = HumanState.GoToDemon;

                
            }
        }
    }
}