using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Core
{
    /// <summary>
    /// Работа с завершением игры
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class EndingManager : MonoBehaviour
    {
        #region Singleton

        public static EndingManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public GameObject endingPanel;
        public Text endingText;
        private AudioSource _audioSource;

        public int winReputation = 300;

        public AudioClip[] tracks;

        private void SetTrack(int trackNum)
        {
            _audioSource.clip = tracks[trackNum];
            _audioSource.Play();
        }

        public string goodEnding;
        public string badEndingReputationZero;
        public string badEndingReputationLow;

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public void LoadEnding(string endingTextString)
        {
            endingPanel.SetActive(true);
            endingText.text = endingTextString;

            // initialize panel
        }


        /// <summary>
        /// Просчитать резульатат окончания игры
        /// </summary>
        public void CalculateEnding()
        {
            var isPositiveEnding = false;

            // Окончание по истечению времени 
            if (TimeManager.Instance.CurrentTime >= TimeManager.Instance.finalTime)
            {
                // Достигнута необходимая репутация
                if (Demon.Instance.Reputation >= winReputation)
                {
                    LoadEnding(goodEnding);
                    TimeManager.Instance.Pause();
                    isPositiveEnding = true;
                }
                else
                {
                    LoadEnding(badEndingReputationLow);
                    TimeManager.Instance.Pause();
                    isPositiveEnding = false;
                }
            }
            else
            {
                if (Demon.Instance.Reputation < 0)
                {
                    LoadEnding(badEndingReputationZero);
                    TimeManager.Instance.Pause();
                    isPositiveEnding = false;
                }
            }

            // Включить звук окончания
            if (isPositiveEnding)
                SetTrack(0);
            else
                SetTrack(1);
        }
    }
}