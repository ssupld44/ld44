using UnityEngine;

namespace Client.Scripts.Core
{
    /// <summary>
    /// Менеджер работы с персонажами
    /// </summary>
    public class HumanManger : MonoBehaviour
    {
        #region Singleton

        public static HumanManger Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            if (Instance != this)
                Destroy(gameObject);
            _humans = Resources.LoadAll<Human>("");
        }

        #endregion

        private Human[] _humans;

        public void ReduceHealth()
        {
            foreach (var human in _humans)
            {
                if (!human.IsDead)
                {
                    human.TimeToDeath--;
                    if (human.TimeToDeath <= 0)
                    {
                        human.IsDead = true;
                        human.TimeToDeath = 0;
                    }
                }
            }
        }
    }
}