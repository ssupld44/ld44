﻿using System.Collections;
using Client.Scripts.Ui.Time;
using UnityEngine;

namespace Client.Scripts.Core
{
    public class TimeManager : MonoBehaviour
    {
        #region Singleton

        public static TimeManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        #region Time

        [Header("Time")]
        public int initialTime = 1970;

        public int finalTime = 2000;

        private int _currentTime;

        // Раз в сколько секунд обновляется время
        public float yearSecondsUpdate = 1.0f;
        public float gameTimeScale = 1f;

        public int CurrentTime
        {
            get => _currentTime;
            private set
            {
                _currentTime = value;
                if (_currentTime >= finalTime)
                {
                    // Отыграть поражение
                    Instance.Pause();
                    EndingManager.Instance.CalculateEnding();
                }

                // Убавить здоровье всем персонажам
                HumanManger.Instance.ReduceHealth();

                // Обновить UI
                TimeController.Instance.CurrentTime = _currentTime;
            }
        }

        #endregion

        private bool _isPaused = false;

        public bool IsPaused => _isPaused;

        private void Start()
        {
            CurrentTime = initialTime - 1;

            StartCoroutine(UpdateTime());

            // Временная правка для на уменьшения количества взаимодейтсвий
             Time.timeScale = gameTimeScale;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!_isPaused)
                    Pause();
                else
                    Unpause();
            }
        }

        private IEnumerator UpdateTime()
        {
            while (true)
            {
                CurrentTime++;
                yield return new WaitForSeconds(yearSecondsUpdate);
            }
        }

        /// <summary>
        /// Вызвать активную пазу (триггер)
        /// </summary>
        public void Pause()
        {
            _isPaused = true;
            ApplyPause();
        }

        public void Unpause()
        {
            _isPaused = false;
            ApplyPause();
        }

        public void ApplyPause()
        {
            Time.timeScale = _isPaused ? 0.0f : 1.0f;
        }
    }
}