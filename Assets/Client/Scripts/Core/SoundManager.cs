﻿using UnityEngine;

namespace Client.Scripts.Core
{
    public class SoundManager : MonoBehaviour
    {
        #region Singleton

        public static SoundManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        private AudioSource _audioSource;
        public AudioClip[] sounds;

        public AudioSource mouseClickAudioSource;
        
        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();

            mouseClickAudioSource.clip = sounds[3];
        }

        private void SetTrack(int trackNum)
        {
            _audioSource.clip = sounds[trackNum];
            _audioSource.Play();
        }

        public void PlaySoundConversation()
        {
            SetTrack(0);
        }

        public void PlaySoundDefeat()
        {
            SetTrack(1);
        }

        public void PlaySoundFailure()
        {
            SetTrack(2);
        }

        public void PlaySoundMouseClick()
        {
            mouseClickAudioSource.Play();
        }

        public void PlaySoundOpenNewspaper()
        {
            SetTrack(4);
        }

        public void PlaySoundSuccess()
        {
            SetTrack(5);
        }

        public void PlaySoundSummonTheDemon()
        {
            SetTrack(6);
        }

        public void PlaySoundTypeWriter()
        {
            SetTrack(7);
        }
        
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                PlaySoundMouseClick();
            }
        }
    }
}