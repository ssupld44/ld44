using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Core
{
    /// <summary>
    /// Главный класс по упрвавлению демоном
    /// </summary>
    public class Demon : MonoBehaviour
    {
        #region Reputation

        [Header("Reputation")]
        public int initialReputation;
        

        private int _reputation;
        public int reputationGainFromDeath = 5;
        public Text UIReputationIndicator;

        public int Reputation
        {
            get => _reputation;
            set
            {
                _reputation = value;
                // Обновить интерфейс
                UIReputationIndicator.text = "Hell Reputation: " +_reputation.ToString();
                if (_reputation < 0)
                {
                    // Отыграть проигрыш
                    EndingManager.Instance.CalculateEnding();
                }
            }
        }

        #endregion

        public static Demon Instance { get; private set; } = null;

        private void Start()
        {
            Reputation = initialReputation;
        }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);

            UIReputationIndicator.text = Reputation.ToString();
            
        }
    }
}