using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts.Core
{
    public class GameManager : MonoBehaviour
    {
        public GameObject pauseMenuHolder;

        public void OnPauseButtonClick()
        {
            pauseMenuHolder.SetActive(true);
            TimeManager.Instance.Pause();
        }

        public void OnContinueButtonClick()
        {
            pauseMenuHolder.SetActive(false);
            TimeManager.Instance.Unpause();
        }

        public void OnRestartButtonClick()
        {
            SceneManager.LoadScene("GameScene");
        }

        public void OnExitButtonClick()
        {
            MusicManager.Instance.SetTrack(0);
            SceneManager.LoadScene("MainMenu");
        }
    }
}