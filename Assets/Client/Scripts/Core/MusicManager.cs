using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts.Core
{
    public class MusicManager : MonoBehaviour
    {
        #region Singleton

        public static MusicManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }

        #endregion

        private AudioSource _audioSource;

        public AudioClip[] tracks;
        public float timeToChange = 0.1f;

        [Range(0.0f, 1.0f)]
        public float volume = 0.5f;

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();

            _audioSource.volume = volume;

            switch (SceneManager.GetActiveScene().name)
            {
                case "GameScene":
                    SetTrack(1);
                    break;
                case "MainMenu":
                    SetTrack(0);
                    break;
            }
        }

        public void SetTrack(int trackNum)
        {
            StartCoroutine(ChangeMusic(trackNum));
        }

        private IEnumerator ChangeMusic(int trackNum)
        {
            while (_audioSource.volume > 0)
            {
                _audioSource.volume -= volume / timeToChange * Time.deltaTime;
                yield return null;
            }

            _audioSource.clip = tracks[trackNum];
            _audioSource.Play();
            while (_audioSource.volume < volume)
            {
                _audioSource.volume += volume / timeToChange * Time.deltaTime;
                yield return null;
            }
        }
    }
}