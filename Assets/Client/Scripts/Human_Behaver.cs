﻿using Client.Scripts;
using Client.Scripts.Core;
using UnityEngine;

public class Human_Behaver : MonoBehaviour
{
    public Human ThisHuman;

    private void Update()
    {
        //Обрабатываем смерть персонажа, если его время жизни истекло
        if (ThisHuman.TimeToDeath <= 0)
        {
            EventResultsApplier.Instance.AddReputationOnDeath();
            //Demon.Instance.Reputation += 5;
            Destroy(this.gameObject);
        }
    }
}