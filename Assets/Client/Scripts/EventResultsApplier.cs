﻿using Client.Scripts.Core;
using UnityEngine;

namespace Client.Scripts
{
    public class EventResultsApplier : MonoBehaviour
    {
        public static EventResultsApplier Instance { get; private set; } = null;

        private void Awake()
        {
            Instance = this;
        }

        public void ApplyResults(Human human, Traits traitPlus = null, Traits traitMinus = null, int timeToDeath = 0,
            int reputation = 0)
        {
            if (traitPlus != null && !human.OwnTraits.Contains(traitPlus))
            {
                human.OwnTraits.Add(traitPlus);
            }


            if (traitMinus != null)
            {
                human.OwnTraits.Remove(traitMinus);
            }

            human.TimeToDeath += timeToDeath;
            Demon.Instance.Reputation += reputation;
        }

        public void AddReputationOnDeath()
        {
            Demon.Instance.Reputation += Demon.Instance.reputationGainFromDeath;
        }
    }
}