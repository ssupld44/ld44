﻿using UnityEngine;

namespace Client.Scripts
{
    public class EventHandler : MonoBehaviour
    {
        #region Singleton

        public static EventHandler Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public GameObject UIEventParentObject;

        public void StartEvent(Human human1, Human human2)
        {
//            var oneEvent = EventFinder.instance.FindEvent(human1, human2);
            UIEventParentObject.SetActive(true);
//            EventDisplay.Instance.DisplayEvent(oneEvent, human1, human2);
//            TimeManager.Instance.Pause();
        }
    }
}