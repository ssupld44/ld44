﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Client.Scripts.Core;

public class TutorBehavior : MonoBehaviour
{
    public Text tutorText;
    public Image imageTutor;
    int counter = 0;

    public List<TutorData> tutorData;

    private void Start()
    {
        LoadNext();
    }

    public void LoadNext()
    {
        if (counter==9)
        {
            SceneManager.LoadScene("GameScene");
            MusicManager.Instance.SetTrack(1);
        }
        Load(counter);
        counter++;
    }

    public void Load (int index)
    {
        imageTutor.sprite = tutorData[index].thisSprite;
        tutorText.text = tutorData[index].text;
    }



}
[Serializable]
public class TutorData
{
    public string text;
    public Sprite thisSprite;
    public bool played = false;
}
