﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Client.Scripts.MovementAI
{
    public enum HumanState
    {
        Undefined = 0,

        // Нулевое состояние
        Idle,

        // Движется к цели интереса
        GoToPoi,

        // Идёт на разговор
        GoToNearTarget,

        // Разговаривает
        Speak,

        // Идёт к статуи
        GoToDemon,

        // Разговаривает с демоном
        TalkToDemon,
    }

    public class MovementSystem : MonoBehaviour
    {
        private NavMeshAgent _navMeshAgent;
        private Animator _animator;
        private SphereCollider _collider;

        public Transform targetMarker;
        public Vector3 nearTarget;
        public GameObject InterestGroup;
        private List<GameObject> InterestPointList = new List<GameObject>();
		private GameObject[] meeting_points;

        public GameObject _lastInteraction;
        private List<GameObject> _latestInteractions = new List<GameObject>();

        #region Stand

        [Header("Stand")]
        public float timeToStandSeconds = 5.0f;

        #endregion

        private HumanState _state;
        private static readonly int IsPraying = Animator.StringToHash("IsPraying");

        public HumanState State
        {
            get => _state;
            set
            {
                _state = value;
                switch (_state)
                {
                    case HumanState.Idle:
                        // Включить коллайдер обнаружения
                        _collider.enabled = true;
                        // Найти новую точку для движения
                        GoToRandomPoi();
                        break;
                    case HumanState.Speak:
                        // Начать общение
                        StartSpeaking();
                        break;
                    case HumanState.GoToDemon:
						// Отправиться к демону
						_collider.enabled = true;
						GoToDemon();
                        break;
                    case HumanState.TalkToDemon:
                        // Начать молиться
                        StartPraying();
                        break;
                    case HumanState.Undefined:
                        Debug.LogWarning("Состояние не назначено");
                        break;
                }
            }
        }

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _collider = gameObject.GetComponent<SphereCollider>();

            // Загрузить все точки интереса
            GetPointOfInterest();
            // Перейти в нулевое состояние
            State = HumanState.Idle;

			meeting_points = GameObject.FindGameObjectsWithTag("MeetingPoint");

		}

        private void Update()
        {
            if (State == HumanState.GoToPoi)
                if (Vector3.Distance(transform.position, _navMeshAgent.destination) < 1.0f)
                    GoToRandomPoi();

			_animator.SetFloat("Speed", _navMeshAgent.velocity.magnitude);


			if (State == HumanState.GoToDemon)
			{
				float dist = Vector3.Distance(transform.position, _navMeshAgent.destination);				
				if (dist < 3)
					State = HumanState.TalkToDemon;
			}

		}

        private bool IsInLastInteractions(GameObject go)
        {
            return _latestInteractions.GetRange(
                Mathf.Clamp(_latestInteractions.Count - 3, 0, _latestInteractions.Count),
                _latestInteractions.Count).Contains(go);
        }

		private void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("Humans"))
			{
				// Не из недавнего списка
				if (!IsInLastInteractions(other.gameObject) && State == HumanState.GoToPoi && other.gameObject.GetComponent<MovementSystem>().State == HumanState.GoToPoi)
				{
					_lastInteraction = other.gameObject;
					other.gameObject.GetComponent<MovementSystem>()._lastInteraction = gameObject;
					Vector3 meeting_position = meeting_points[Random.Range(0, meeting_points.Length)].transform.position;
					GoToNearMan(meeting_position);
					other.gameObject.GetComponent<MovementSystem>().GoToNearMan(meeting_position);
				}
			}
		}

        private void GetPointOfInterest()
        {
            foreach (Transform child in InterestGroup.transform)
            {
                InterestPointList.Add(child.gameObject);
            }
        }

        private Transform GetRandomPoi()
        {
            return InterestPointList[Random.Range(0, InterestPointList.Count - 1)].transform;
        }

        private void UpdateTargets(Vector3 targetPosition)
        {
            _navMeshAgent.destination = targetPosition;
        }

        private void StartSpeaking()
        {
            // Запустить анимацию
            _animator.Play("Speaking");
            // Запустить звку разговора
            // TODO: звук разговора
            // Остановить движение
            _navMeshAgent.isStopped = true;
            // Ожидать окончания
            StartCoroutine(WaitForSpeaking(5f));
        }

        private IEnumerator WaitForSpeaking(float talkTime)
        {
            // Ждать разговора
            yield return new WaitForSeconds(talkTime);

            // Закончить разговор
            _animator.Play("Walking");
            gameObject.GetComponent<NavMeshAgent>().isStopped = false;
            if (_lastInteraction != null)
            {
                _latestInteractions.Add(_lastInteraction);
                _lastInteraction = null;
            }

            // Перейти в нулевое состояние
            State = HumanState.Idle;
        }

        private void GoToDemon()
        {
            // Выбор статуи
            var statues = GameObject.FindGameObjectsWithTag("Statue_1");
            if (statues == null)
            {
                State = HumanState.Idle;
                return;
            }

            // Движение к статуи
            _navMeshAgent.destination = statues[0].GetComponent<Collider>().bounds.center;
        }

        private void StartPraying()
        {
            // Проиграть анимацию
            _animator.SetBool(IsPraying, true);
            // Остановить движение
            _navMeshAgent.isStopped = true;
            // Создать знак
            GetComponent<ColliderReaction>().CreateWarningMarker();
            // Ждать ответа на запрос
            StartCoroutine(WaitForRequestResponse(5.0f));
        }

        private IEnumerator WaitForRequestResponse(float responseTime)
        {
            // Ждать время ответа
            yield return new WaitForSeconds(responseTime);
            // Cменить анимацию
            _animator.SetBool(IsPraying, false);
            // Начать двидение
            _navMeshAgent.isStopped = false;
            // Перейти в нулевое состояние
            State = HumanState.Idle;
        }

        /// <summary>
        /// Идти в случайную точку
        /// </summary>
        private void GoToRandomPoi()
        {
            GoToPoi(GetRandomPoi());
        }

        /// <summary>
        /// Идти в точку интереса
        /// </summary>
        /// <param name="destination"></param>
        private void GoToPoi(Transform destination)
        {
            UpdateTargets(destination.position);
            State = HumanState.GoToPoi;
        }

        public void GoToNearMan(Vector3 destination)
        {
            UpdateTargets(destination);
			State = HumanState.GoToNearTarget;
			_collider.enabled = false;
		}
    }
}