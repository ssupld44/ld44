﻿using Client.Scripts.MovementAI;
using UnityEngine;

public class FindNearMan : MonoBehaviour
{
    public GameObject[] Humans;
    public GameObject Closest;

    private void Start()
    {
        Humans = GameObject.FindGameObjectsWithTag("Humans");
    }

    public void FindClosest(GameObject[] Humans)
    {
        GameObject closestO = null;
        float distance = Mathf.Infinity;

        foreach (GameObject go in Humans)
        {
            float curDistance = Vector3.Distance(transform.position, go.transform.position);
            if (curDistance < distance && go != gameObject)
            {
                closestO = go;
                distance = curDistance;
                // Debug.Log(closestO);
                // Debug.Log(distance);
                this.gameObject.GetComponent<MovementSystem>().nearTarget = closestO.transform.position;
            }
        }
    }
}