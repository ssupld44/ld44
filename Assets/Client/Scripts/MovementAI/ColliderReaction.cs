﻿using UnityEngine;

namespace Client.Scripts.MovementAI
{
    public class ColliderReaction : MonoBehaviour
    {
        public bool haveindicate = false;

        public GameObject Question1;
        public GameObject Warning1;

        private void colliderreact(Collision other)
        {
            var firstHuman = gameObject;
            var secondHuman = other.gameObject;

            // Сменяем состояние на разговор
            var firstHumanMS = firstHuman.GetComponent<MovementSystem>();
            var secondHumanMS = secondHuman.GetComponent<MovementSystem>();

            firstHumanMS.State = HumanState.Speak;
            secondHumanMS.State = HumanState.Speak;

            // Передаём собитие обработчику
            var human1 = firstHuman.GetComponent<Human_Behaver>().ThisHuman;
            var human2 = secondHuman.GetComponent<Human_Behaver>().ThisHuman;
            GameObject mark = Instantiate(Question1, gameObject.transform.position, gameObject.transform.rotation);
            EventHandler.Instance.StartEvent(human1, human2);
            Bubble_script bubbleScript = mark.GetComponent<Bubble_script>();
            bubbleScript.human1 = firstHuman;
            bubbleScript.human2 = secondHuman;
            bubbleScript.thisEvent = EventFinder.Instance.FindEvent(human1, human2);
            bubbleScript.ApplyResultsOfEvent();
        }

        public void CreateWarningMarker()
        {
            var warningBubbleGo = Instantiate(Warning1, gameObject.transform.position, gameObject.transform.rotation);
            warningBubbleGo.GetComponent<Warning>().Initialize(gameObject);
        }

        private void OnCollisionEnter(Collision other)
        {
            // Поговорить
            if (other.gameObject.CompareTag("Humans"))
            {
				if (GetComponent<MovementSystem>()._lastInteraction == other.gameObject)
				{
					var firstHumanMS = this.gameObject.GetComponent<MovementSystem>();
					var secondHumanMS = other.gameObject.GetComponent<MovementSystem>();

					if (other.gameObject.GetComponent<ColliderReaction>().haveindicate == false &&
						firstHumanMS.State != HumanState.Speak && (secondHumanMS.State != HumanState.Speak))
					{
						transform.LookAt(other.gameObject.transform.position);
						transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

						other.gameObject.transform.LookAt(transform.position);
						other.gameObject.transform.eulerAngles = new Vector3(0, other.gameObject.transform.eulerAngles.y, 0);

						haveindicate = true;
						colliderreact(other);
					}
				}
            }
            else if (other.gameObject.CompareTag("Statue_1"))
            {
                var humanMs = GetComponent<MovementSystem>();
                if (humanMs)
                    humanMs.State = HumanState.TalkToDemon;
            }
            else
            {
                Debug.Log("Its not a Humans");
            }
        }
    }
}