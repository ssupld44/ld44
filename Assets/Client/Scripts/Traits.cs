﻿using UnityEngine;

namespace Client.Scripts
{
    [CreateAssetMenu(fileName = "New Trait", menuName = "Custom/Trait")]
    public class Traits : ScriptableObject
    {
        //Имя характеристики
        public string name;

        //Описание характеристики
        public string description;
    }
}