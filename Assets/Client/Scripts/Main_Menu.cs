﻿using Client.Scripts.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts
{
    public class Main_Menu : MonoBehaviour
    {
        public GameObject start_lights;
        public GameObject exit_ligths;
        public GameObject logo_lights;
        public GameObject credits_lights;

        public Collider start_collider;
        public Collider exit_collider;
        public Collider credits_collider;

        public Material lights;
        public Material lights_off;

        MeshRenderer exit_renderer;
        MeshRenderer start_renderer;
        MeshRenderer credits_renderer;

        #region Credits

        [Header("Credits")]
        public GameObject creditsPanel;

        private bool _creditsShown = false;

        #endregion

        private void Start()
        {
            start_lights.GetComponent<MeshRenderer>().material = lights;
            exit_ligths.GetComponent<MeshRenderer>().material = lights;
            logo_lights.GetComponent<MeshRenderer>().material = lights;
            credits_lights.GetComponent<MeshRenderer>().material = lights;

            exit_renderer = exit_ligths.GetComponent<MeshRenderer>();
            start_renderer = start_lights.GetComponent<MeshRenderer>();
            credits_renderer = credits_lights.GetComponent<MeshRenderer>();

            exit_renderer.material = lights_off;
            start_renderer.material = lights_off;
            credits_renderer.material = lights_off;

            HideCredits();
        }

        private void Update()
        {
            if (_creditsShown)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    HideCredits();
                }
            }

            // пускаем луч в место установки
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit))
            {
                if (hit.collider.name == start_collider.name)
                {
                    start_renderer.material = lights;
                    if (Input.GetMouseButtonDown(0))
                    {
                        SceneManager.LoadScene("Tutor");
                        // MusicManager.Instance.SetTrack(1);
                    }
                }
                else
                {
                    start_renderer.material = lights_off;
                }

                if (hit.collider.name == exit_collider.name)
                {
                    exit_renderer.material = lights;
                    if (Input.GetMouseButtonDown(0))
                    {
                        Application.Quit();
                    }
                }
                else
                {
                    exit_renderer.material = lights_off;
                }

                if (hit.collider.name == credits_collider.name)
                {
                    credits_renderer.material = lights;
                    if (Input.GetMouseButtonDown(0))
                    {
                        ShowCredits();
                        credits_renderer.material = lights;
                    }
                }
                else
                {
                        credits_renderer.material = lights_off;
                }
            }
            else
            {
                credits_renderer.material = lights_off;
            }
        }

        private void ShowCredits()
        {
            creditsPanel.SetActive(true);
            _creditsShown = true;
            Time.timeScale = 0.0f;
        }

        private void HideCredits()
        {
            creditsPanel.SetActive(false);
            _creditsShown = false;
            Time.timeScale = 1.0f;
        }
    }
}