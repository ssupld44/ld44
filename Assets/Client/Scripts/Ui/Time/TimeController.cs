﻿using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Ui.Time
{
    public class TimeController : MonoBehaviour
    {
        #region Singleton 

        public static TimeController Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        [Header("UI")]
        public Text currentTimeText;

        public int CurrentTime
        {
            set => currentTimeText.text = "Year: " + value.ToString();
        }
    }
}