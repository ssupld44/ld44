﻿using Client.Scripts.Ui.Logging.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Ui.Logging.View
{
    public class LogEntry : MonoBehaviour
    {
        public Text timeText;
        public Text infoText;

        public void Initialize(LogEntryData data)
        {
            timeText.text = data.Time.ToString("yyyy MMMM dd");
            infoText.text = data.Info;
        }
    }
}