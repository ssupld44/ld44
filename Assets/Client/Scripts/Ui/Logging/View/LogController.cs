﻿using Client.Scripts.Core;
using Client.Scripts.Ui.Logging.Model;
using UnityEngine;

namespace Client.Scripts.Ui.Logging.View
{
    public class LogController : MonoBehaviour
    {
        #region  Singleton

        public static LogController Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public GameObject logEntryPrefab;

        public Transform logEntryHolder;

        private Animator _animator;
        
        private static readonly int OpenedAnimationParamHash = Animator.StringToHash("Opened");

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        /// <summary>
        /// Добавить запись в лог
        /// </summary>
        /// <param name="logEntryData">Данные для записи</param>
        public void AddLogEntry(LogEntryData logEntryData)
        {
            // Добавить объект
            var logEntryGo = Instantiate(logEntryPrefab, logEntryHolder);
            logEntryGo.transform.SetAsFirstSibling();
            logEntryGo.GetComponent<LogEntry>().Initialize(logEntryData);
            // Воспроизвести звук
            SoundManager.Instance.PlaySoundTypeWriter();
        }

        public void Update()
        {
            if (Screen.width * 0.9 < Input.mousePosition.x && Input.mousePosition.x < Screen.width)
            {
                // Показать лог
                _animator.SetBool(OpenedAnimationParamHash, true);
            }
            else
            {
                // Спрятать лог
                _animator.SetBool(OpenedAnimationParamHash, false);
            }
        }
    }
}