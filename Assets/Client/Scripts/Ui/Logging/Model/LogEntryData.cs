﻿using System;

namespace Client.Scripts.Ui.Logging.Model
{
    /// <summary>
    /// Данные для записи в лог 
    /// </summary>
    public class LogEntryData
    {
        // Время создания
        public DateTime Time;

        // Текст информации
        public string Info;
    }
}