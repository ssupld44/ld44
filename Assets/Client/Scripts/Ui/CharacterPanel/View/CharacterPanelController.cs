﻿using Client.Scripts.Core;
using Client.Scripts.Ui.CharacterPanel.Model;
using UnityEngine;

namespace Client.Scripts.Ui.CharacterPanel.View
{
    public class CharacterPanelController : MonoBehaviour
    {
        #region Singleton

        public static CharacterPanelController Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public GameObject characterPanel;
        private CharacterPanel _characterPanel;

        private Camera _avatarCamera;
        
        private void Start()
        {
            _characterPanel = characterPanel.GetComponent<CharacterPanel>();

            // Спрятать панель
            HidePanel();
        }

        /// <summary>
        /// Показать панель
        /// </summary>
        /// <param name="characterData"></param>
        /// <param name="avatarTexture"></param>
        /// <param name="avatarCamera"></param>
        public void ShowPanel(CharacterData characterData, Texture avatarTexture, Camera avatarCamera)
        {
            characterPanel.SetActive(true);
            _avatarCamera = avatarCamera;
            _avatarCamera.enabled = true;
            _characterPanel.ShowPanel(characterData, avatarTexture);
            
            TimeManager.Instance.Pause();
        }

        /// <summary>
        /// Спрятать панель
        /// </summary>
        public void HidePanel()
        {
            characterPanel.SetActive(false);
            if (_avatarCamera)
                _avatarCamera.enabled = false;
            TimeManager.Instance.Unpause();
        }
    }
}