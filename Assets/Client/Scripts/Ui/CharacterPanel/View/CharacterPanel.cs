﻿using Client.Scripts.Core;
using Client.Scripts.Ui.CharacterPanel.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Ui.CharacterPanel.View
{
    public class CharacterPanel : MonoBehaviour
    {
        #region Ui

        [Header("UI")]
        public Text nameLabel;

        public Text descriptionsLabel;
        public Text traitsLabel;
        public Text timeToDeathLabel;
        public Text moralLabel;
        public RawImage portrait;
        public Button hideButton;

        #endregion

        private Texture _textureFromCamera;

        private void Start()
        {
            hideButton.onClick.AddListener(() => { CharacterPanelController.Instance.HidePanel(); });
            HidePanel();
        }

        /// <summary>
        /// Показать панель
        /// </summary>
        /// <param name="data"></param>
        /// <param name="avatarTexture"></param>
        public void ShowPanel(CharacterData data, Texture avatarTexture)
        {
            // Заполнить панель
            FillPanel(data, avatarTexture);
        }

        public void HidePanel()
        {
            CharacterPanelController.Instance.HidePanel();
        }

        public void FillPanel(CharacterData data, Texture avatarTexture)
        {
            nameLabel.text = $"Name: {data.CharacterName}";
            var description = $"Description: {data.Description}";
            var traitsToLabel = "Traits: ";
            if (data.Traits != null)
            {
                foreach (var trait in data.Traits)
                {
                    if (trait != null)
                    {
                        traitsToLabel = traitsToLabel + trait.name + ". ";
                        description += trait.description;
                    }
                }
            }
            else
            {
                Debug.LogWarning("Пустой Хуман трейт");
                Debug.Log(data.Traits);
            }

            traitsLabel.text = traitsToLabel;
            descriptionsLabel.text = description;
            moralLabel.text = $"Moral points: ${data.Moral}";
            timeToDeathLabel.text = $"Time to death: {data.TimeToDeath}";
            portrait.texture = avatarTexture;
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                HidePanel();
            }
        }
    }
}