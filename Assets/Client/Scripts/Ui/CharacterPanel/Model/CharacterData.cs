using System.Collections.Generic;

namespace Client.Scripts.Ui.CharacterPanel.Model
{
    public class CharacterData
    {
        public string CharacterName;
        public string Description;
        public List<Traits> Traits;
        public int TimeToDeath;
        public float Moral;
    }
}