﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Client.Scripts.Ui.Request.Model
{
    [CreateAssetMenu(fileName = "New RequestData", menuName = "Custom/Request")]
    public class RequestData : ScriptableObject
    {

        public Traits requirement;

        public string requestName;
        public string description;
        public List<ChoiceData> choices;
    }
    
    [Serializable]
    public class ChoiceData
    {
        public string name;
        public Traits traitToAdd;
        public Traits traitToRemove;
        public int reputationDiff;
        public int healthDiff;
    }
}
