﻿using System.Collections.Generic;
using Client.Scripts.Ui.Request.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Ui.Request.View
{
    public class Request : MonoBehaviour
    {
        [Header("UI")]
        public Text nameText;

        public Text descriptionText;

        [Header("Choices")]
        public GameObject choiceUiPrefab;

        public Transform choiceHolder;

        private List<GameObject> _choices = new List<GameObject>();
        
        public void ShowPanel(Human human, RequestData data)
        {
            nameText.text = human.name;
            descriptionText.text = data.description;

            data.choices.ForEach(choiceData =>
            {
                
                var choiceGo =Instantiate(choiceUiPrefab, choiceHolder);
                choiceGo.GetComponent<Choice>().Initialize(human, choiceData);
                _choices.Add(choiceGo);
            });
        }

        public void HidePanel()
        {
            _choices.ForEach(Destroy);
            _choices.Clear();
        }
    }
}