﻿using Client.Scripts.Core;
using Client.Scripts.Ui.Request.Model;
using UnityEngine;

namespace Client.Scripts.Ui.Request.View
{
    public class RequestManager : MonoBehaviour
    {
        public static RequestManager Instance { get; private set; } = null;

        [Header("UI")]
        public GameObject requestHolder;

        public GameObject requestObject;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);

            Deactivate();
        }

        public void Activate(Human humanData, RequestData requestData)
        {
            requestObject.SetActive(true);
            requestObject.GetComponent<Request>().ShowPanel(humanData, requestData);
            TimeManager.Instance.Pause();
        }

        public void Deactivate()
        {
            requestHolder.SetActive(false);
            requestObject.GetComponent<Request>().HidePanel();
            TimeManager.Instance.Unpause();
        }
    }
}
