﻿using Client.Scripts.Ui.Request.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Ui.Request.View
{
    public class Choice : MonoBehaviour
    {
        [Header("Ui")]
        public Text choiceName;

        public Text reputationText;
        public Text healthText;

        public Button button;

        public void Initialize(Human human, ChoiceData data)
        {
            choiceName.text = data.name;
            reputationText.text = $"{data.reputationDiff:+0;-#}";
            healthText.text = $"{data.healthDiff:+0;-#}";

            button.onClick.AddListener(() =>
            {
                EventResultsApplier.Instance.ApplyResults(human, data.traitToAdd, data.traitToRemove,
                    data.healthDiff, data.reputationDiff);
                RequestManager.Instance.Deactivate();
            });
        }
    }
}