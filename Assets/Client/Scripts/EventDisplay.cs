﻿using Client.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;
using Client.Scripts.Ui.Logging.View;
using Client.Scripts.Ui.Logging.Model;

namespace Client.Scripts
{
    public class EventDisplay : MonoBehaviour
    {
        #region Singleton

        public static EventDisplay Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) Destroy(gameObject);
        }

        #endregion

        public Text nameText;
        public Text descriptionText;
        public GameObject UIEventDisplayPanel;
        public GameObject parentObject;

        private void Start()
        {
            HideDisplay();
        }

        public void DisplayEvent(Events oneEvent, Human human1, Human human2 = null)
        {
            parentObject.SetActive(true);
            UIEventDisplayPanel.SetActive(true);
            nameText.text = oneEvent.name;
            descriptionText.text = TextConstructor.Instance.ConstructText(oneEvent, human1, human2);

            EventResultsApplier.Instance.ApplyResults(human1, oneEvent.Traits_Plus_H1, oneEvent.Traits_Minus_H1,
                oneEvent.age_change_human1, oneEvent.reputationChange);
            if (human2 != null)
            {
                EventResultsApplier.Instance.ApplyResults(human2, oneEvent.Traits_Plus_H2, oneEvent.Traits_Minus_H2,
                    oneEvent.age_change_human2);
            }

            LogController.Instance.AddLogEntry(new LogEntryData
            {
                Time = System.DateTime.Now,
                Info = TextConstructor.Instance.ConstructText(oneEvent, human1, human2)
            });

            TimeManager.Instance.Pause();
        }

        public void NextClick()
        {
            HideDisplay();
            TimeManager.Instance.Unpause();
        }

        public void HideDisplay()
        {
            UIEventDisplayPanel.SetActive(false);
            parentObject.SetActive(false);
        }
    }
}