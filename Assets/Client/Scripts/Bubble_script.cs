﻿using System.Collections;
using Client.Scripts;
using Client.Scripts.MovementAI;
using UnityEngine;

public class Bubble_script : MonoBehaviour
{
    public GameObject human1;
    public GameObject human2;
    public Events thisEvent;
    public float secBeforeDestroy = 15f;
    public float timeOfStart;

    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();

        timeOfStart = Time.time;

        StopCoroutine(PlayDestroyAnimation());
    }

    public void OnMouseDown()
    {
        Destroy(gameObject);
        EventDisplay.Instance.DisplayEvent(thisEvent, human1.gameObject.GetComponent<Human_Behaver>().ThisHuman,
            human2.gameObject.GetComponent<Human_Behaver>().ThisHuman);

    }

    public void ApplyResultsOfEvent()
    {
        EventResultsApplier.Instance.ApplyResults(human1.gameObject.GetComponent<Human_Behaver>().ThisHuman,
            thisEvent.Traits_Plus_H1, thisEvent.Traits_Minus_H1,
            thisEvent.age_change_human1, thisEvent.reputationChange);
        if (human2 != null)
        {
            EventResultsApplier.Instance.ApplyResults(human2.gameObject.GetComponent<Human_Behaver>().ThisHuman,
                thisEvent.Traits_Plus_H2, thisEvent.Traits_Minus_H2,
                thisEvent.age_change_human2);
        }
    }

    private void Update()
    {
        if (Time.time - timeOfStart >= secBeforeDestroy)
        {
            ApplyResultsOfEvent();
            human1.gameObject.GetComponent<ColliderReaction>().haveindicate = false;
            human2.gameObject.GetComponent<ColliderReaction>().haveindicate = false;

            Destroy(gameObject);
        }
    }

    private IEnumerator PlayDestroyAnimation()
    {
        yield return new WaitForSeconds(secBeforeDestroy - 5.0f);

        _animator.SetBool("Destroyed", true);
    }
}