﻿using UnityEngine;

namespace Client.Scripts
{
    public class CameraControlNew : MonoBehaviour
    {
        public Transform cameraTransform;
        public Transform centerTransform;

        public float speedZoom = 1.0f;

        private float _angle;

        #region Radius

        [Header("Radius")]
        public float initialRadius = 2.0f;

        private float _currentRadius;
        public float minRadius = 1.0f;
        public float maxRadius = 20.0f;
		

        #endregion

        public float angularSpeed = 2.0f;

        Vector2 old_mouse_position;

        private void Start()
        {
            _currentRadius = initialRadius;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(1))
                old_mouse_position = Input.mousePosition;
            if (Input.GetMouseButton(1))
            {
                float delta = Input.mousePosition.x - old_mouse_position.x;
                old_mouse_position = Input.mousePosition;
				
                _angle += Time.deltaTime * angularSpeed * delta;
				
				if (_angle > 2 * Mathf.PI)
                    _angle -= 2 * Mathf.PI;
                if (_angle < 0)
                    _angle += 2 * Mathf.PI;
            }

            var posX = centerTransform.position.x + Mathf.Sin(_angle) * _currentRadius;
            var posZ = centerTransform.position.z + Mathf.Cos(_angle) * _currentRadius;
			var posY = Mathf.Clamp(_currentRadius, 6, 20);

			cameraTransform.position = new Vector3(posX, posY, posZ);
            cameraTransform.LookAt(centerTransform.position);
                        
            _currentRadius -= speedZoom * Input.mouseScrollDelta.y;
            _currentRadius = Mathf.Clamp(_currentRadius, minRadius, maxRadius);
        }
    }
}