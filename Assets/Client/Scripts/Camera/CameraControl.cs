﻿using UnityEngine;

namespace Client.Scripts
{
    public class CameraControl : MonoBehaviour
    {
        public Transform cameraTransform;
        public Transform centerTransform;

        public float speedZoom = 1.0f;

        private float _angle;

        #region Radius

        [Header("Radius")]
        public float initialRadius = 2.0f;

        private float _currentRadius;
        public float minRadius = 1.0f;
        public float maxRadius = 20.0f;

        #endregion

        public float angularSpeed = 2.0f;

        private void Start()
        {
            _currentRadius = initialRadius;
        }

        private void Update()
        {
            var direction = 0;
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow))
                direction = -1;
            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                direction = 1;

            var posX = centerTransform.position.x + Mathf.Sin(_angle) * _currentRadius;
            var posZ = centerTransform.position.z + Mathf.Cos(_angle) * _currentRadius;
            cameraTransform.position = new Vector3(posX, cameraTransform.position.y, posZ);
            cameraTransform.LookAt(centerTransform.position);

            // Подсчитать новый угол и радиус
            _angle += Time.deltaTime * angularSpeed * direction;
            if (_angle > 360.0f)
                _angle = 0f;

            _currentRadius -= speedZoom * Input.mouseScrollDelta.y;
            _currentRadius = Mathf.Clamp(_currentRadius, minRadius, maxRadius);
        }
    }
}