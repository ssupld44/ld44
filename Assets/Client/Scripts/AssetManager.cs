﻿using Client.Scripts.Ui.Request.Model;
using UnityEngine;

namespace Client.Scripts
{
    public class AssetManager : MonoBehaviour
    {
        public static AssetManager Instance { get; private set; } = null;

        public Events[] events;
        public Traits[] traits;
        public Human[] humans;
        public RequestData[] requests;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);

            Initialize();
        }

        private void Initialize()
        {
            events = Resources.LoadAll<Events>("");
            traits = Resources.LoadAll<Traits>("");
            humans = Resources.LoadAll<Human>("");
            requests = Resources.LoadAll<RequestData>("");
            SetDefaultValuesHuman();
        }

        public void SetDefaultValuesHuman()
        {
            foreach (var oneHuman in humans)
            {
                oneHuman.OwnTraits = oneHuman.DefaultOwnTraits;
                oneHuman.TimeToDeath = oneHuman.DefaultTimeToDeath;
            }
        }
    }
}