﻿using System.Collections;
using Client.Scripts.Core;
using Client.Scripts.MovementAI;
using Client.Scripts.Ui.Request.View;
using UnityEngine;

namespace Client.Scripts
{
    public class Warning : MonoBehaviour
    {
        private GameObject _humanGo;

        private float secBeforeDestroy = 15.0f;

        private Animator _animator;

        public void Initialize(GameObject humanGo)
        {
            _humanGo = humanGo;

            StartCoroutine(DestroyAndApplyPenalty());

            StopCoroutine(PlayDestroyAnimation());
        }

        public void OnMouseDown()
        {
            // Выбора запроса
            var requests = AssetManager.Instance.requests;
            var request = requests[Random.Range(0, requests.Length)];

            // Активация запроса
            if (_humanGo == null || request == null)
            {
                Debug.LogError("Cant play request");
                return;
            }

            RequestManager.Instance.Activate(_humanGo.GetComponent<Human_Behaver>().ThisHuman, request);
            _humanGo.GetComponent<MovementSystem>().State = HumanState.Idle;
            Destroy(gameObject);
        }

        private IEnumerator DestroyAndApplyPenalty()
        {
            // Ждать
            yield return new WaitForSeconds(secBeforeDestroy);

            // Наложить штраф
            Demon.Instance.Reputation -= 10;

            // Уничтожить объект
            Destroy(gameObject);
        }

        private IEnumerator PlayDestroyAnimation()
        {
            yield return new WaitForSeconds(secBeforeDestroy - 5.0f);

            _animator.SetBool("Destroyed", true);
        }
    }
}