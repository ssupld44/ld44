﻿using UnityEngine;

namespace Client.Scripts
{
    public class EventTester : MonoBehaviour
    {
        #region Singleton

        public static EventTester Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        [HideInInspector]
        public int counter = 1000;

        public Events[] events;
        public Traits[] traits;
        public Human[] humans;

        private void Start()
        {
            ChooseTwoHumans();
        }

        public void ChooseTwoHumans()
        {
            int counter = 0;
            Human human1 = FindRandomCharacter();
            Human[] humansCopy = humans;

            Human human2;
            do
            {
                counter++;
                if (counter > 100)
                {
                    Debug.Log("Timer is more than 100");
                    return;
                }

                human2 = FindRandomCharacter();
            } while (human1 == human2);

            Events oneEvent = EventFinder.Instance.FindEvent(human1, human2);
            Debug.LogWarning(oneEvent.name + "event name");
            EventDisplay.Instance.DisplayEvent(oneEvent, human1, human2);
        }

        private Human FindRandomCharacter()
        {
            int index = Random.Range(0, humans.Length);
            Debug.Log(index + " index");
            Human human = humans[index];
            Debug.Log(human.name + " human name");
            return human;
        }

        private void Update()
        {
            if (counter > 0)
            {
                ChooseTwoHumans();
                counter--;
            }
        }
    }
}