﻿using System.Collections.Generic;
using UnityEngine;

namespace Client.Scripts
{
    [CreateAssetMenu(menuName = "Custom/Event", fileName = "New Event")]
    public class Events : ScriptableObject
    {
        [TextArea(2,6)]
        public string name, description;
        public List<Traits> traits_req_human1;
        public List<Traits> traits_req_human2;
        public int reputationChange;
        public int age_change_human1, age_change_human2;
        public Traits Traits_Plus_H1, Traits_Minus_H1, Traits_Plus_H2, Traits_Minus_H2;

        public List<Traits> traitListPlusH1;
        public List<Traits> traitListMinusH1;
        public List<Traits> traitListPlusH2;
        public List<Traits> traitListMinusH2;

    }
}
