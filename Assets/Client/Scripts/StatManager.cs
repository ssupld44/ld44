﻿using UnityEngine;

namespace Client.Scripts
{
    public class StatManager : MonoBehaviour
    {
        #region Singleton

        public static StatManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        #region Reputation

        private int _reputation;

        public int Reputation
        {
            get => _reputation;
            set => _reputation = value;
        }

        #endregion
    }
}