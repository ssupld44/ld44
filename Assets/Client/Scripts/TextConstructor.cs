﻿using UnityEngine;

namespace Client.Scripts
{
    public class TextConstructor : MonoBehaviour
    {
        #region Singleton

        public static TextConstructor Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public string ConstructText(Events oneEvent, Human human1, Human human2)
        {
            var text = "";
            text += human1.name + " " + oneEvent.description + " " + human2.name + ". ";
            if (oneEvent.age_change_human1 > 0)
            {
                text += human1.name + "will live longer by " + oneEvent.age_change_human1.ToString() + " years.";
            }

            if (oneEvent.age_change_human1 < 0)
            {
                text += human1.name + " will live shorter life by " + oneEvent.age_change_human1.ToString() + " years.";
            }

            if (oneEvent.age_change_human2 < 0)
            {
                text += human2.name + " will live longer by " + oneEvent.age_change_human2.ToString() +
                        " years. Baaad.";
            }

            if (oneEvent.age_change_human1 < 0)
            {
                text += human2.name + " will live longer by " + oneEvent.age_change_human2.ToString() +
                        " years. Baaad.";
            }

            return text;
        }
    }
}