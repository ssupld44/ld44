﻿using UnityEngine;

namespace Client.Scripts
{
    public class Exit_game_button : MonoBehaviour
    {
        public void OnMouseDown()
        {
            Application.Quit();
        }
    }
}
