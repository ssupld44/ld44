﻿using System.Collections.Generic;
using UnityEngine;

namespace Client.Scripts
{
    public class EventFinder : MonoBehaviour
    {
        #region Singleton

        public static EventFinder Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public Human human1;
        public Human human2;

        public Events[] events;
        public Traits[] traits;
        public Human[] humans;

        private void Start()
        {
            events = AssetManager.Instance.events;
        }

        public void StartEvent(Events oneEvent)
        {
        }

        public Events FindEvent(Human human1, Human human2)
        {
            var readyEvents = new List<Events>();
            foreach (var oneEvent in events)
            {
                if (CheckIfRequirementsAreMet(oneEvent, human1, human2))
                {
                    readyEvents.Add(oneEvent);
                    StartEvent(oneEvent);
//                return oneEvent; 
                }
            }

            if (readyEvents.Count == 0)
            {
                Debug.LogError("No event found");
                return null;
            }

            var targetEvent = readyEvents[Random.Range(0, readyEvents.Count)];
            return targetEvent;
        }

        private bool CheckIfRequirementsAreMet(Events oneEvent, Human human1, Human human2)
        {
            var result = true;
            foreach (var trait in oneEvent.traits_req_human1)
            {
                if (!human1.OwnTraits.Contains(trait))
                    result = true;
            }

            foreach (var trait in oneEvent.traits_req_human2)
            {
                if (!human2.OwnTraits.Contains(trait))
                    result = true;
            }

            return result;
        }
    }
}