﻿using System.Collections.Generic;
using UnityEngine;

namespace Client.Scripts
{
    [CreateAssetMenu(menuName = "Custom/Human", fileName = "New Human")]
    public class Human : ScriptableObject
    {
        //Имя персонажа 
        public string name;

        //Оставшееся время жизни 
        public int DefaultTimeToDeath = 100;

        [HideInInspector]
        public int TimeToDeath = 100;

        //Мораль по умолчанию нейтральная, может быть от -100 до +100 - пока не используем
        public float Moral = 0;

        //Текущие характеристики
        [HideInInspector]
        public List<Traits> OwnTraits;

        public List<Traits> DefaultOwnTraits;

        // Жив ли персонаж
        public bool IsDead = false;
    }
}