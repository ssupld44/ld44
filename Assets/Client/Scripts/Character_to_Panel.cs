﻿using Client.Scripts.Core;
using Client.Scripts.Ui.CharacterPanel.Model;
using Client.Scripts.Ui.CharacterPanel.View;
using UnityEngine;

namespace Client.Scripts
{
    public class Character_to_Panel : MonoBehaviour
    {
        public CharacterPanel characterPanelScript;
        public LayerMask mask;

        private Camera _avatarCamera;
        private Texture _avatarTexture;

        private void Start()
        {
            foreach (Transform child in transform.parent.transform)
            {
                if (child.name == "Character_camera")
                {
                    _avatarCamera = child.GetComponent<Camera>();
                    _avatarCamera.enabled = false;
                    _avatarTexture = _avatarCamera.targetTexture;
                }
            }
        }

        public void Update()
        {
            if (TimeManager.Instance.IsPaused)
                return;

            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var hit, mask))
                {
                    if (hit.collider.gameObject == gameObject)
                    {
                        var humanBehavior = transform.parent.GetComponent<Human_Behaver>();
                        var human = humanBehavior.ThisHuman;
                        CharacterPanelController.Instance.ShowPanel(new CharacterData
                        {
                            CharacterName = human.name,
                            Description = "",
                            TimeToDeath = human.TimeToDeath,
                            Moral = human.Moral,
                            Traits = human.OwnTraits,
                        }, _avatarTexture, _avatarCamera);
                    }
                }
            }
        }
    }
}